#include "face2facedirected.h"


#define MANIFOLD_FLAG "-m"


int main(int argc, char **argv) {

    // check the args to make sure there's an input file
	if (argc == 2) { // two parameters - read a file
		if (!face2facedirected(argv[1], false)) {
			printf("failed \n");
		}
		else {
            printf("done \n");
		}		
	} // two parameters - read a file
	// three parameters - check if 3rd parameter is manifold checking flag
	else if (argc == 3) {
		if (strcmp(argv[2], MANIFOLD_FLAG) == 0) {
			// run face2face with manifold checking
			if (!face2facedirected(argv[1], true)) {
				// printing failed or done does not matter here as manifold checking will print success or failure messages
				//printf("failed \n");
			}
		} 
		// print warning 
		else printf("unexpected third argument \"%s\" used\n => please use \"-m\" to enable manifold checking\n", argv[2]);
	}
	
	else {
		cerr << "Unexpected number of arguments received- please run with: \n 1. first argument supplying the .face file to be converted \n 2. optional second argument '-m' if you wish manifold checking to be enabled \n";
	}

}