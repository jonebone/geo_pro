#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>

using namespace std;

char * read_file(char * path);

pair<string, string> isolate_filename_and_filepath(string fullString);