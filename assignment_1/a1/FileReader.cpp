#include "FileReader.h"

char * read_file(char * path) {
    // open file to read from
    ifstream read_file(path, ifstream::in);
    // error if file cannot be read
    if (read_file.bad()) {
        cerr << "File " << path << " failed to open \n";
        return NULL;
    }

    // seek to end of file
    // constant time
    read_file.seekg(0,read_file.end);
    // report int position of end of file
    int fileLength = read_file.tellg();
    // seek back to start
    read_file.seekg(0, read_file.beg);

    //read file to char array
    // complexity of n where n = fileLength + 1
    char * file_chars = new char[fileLength + 1];
    read_file.read(file_chars, fileLength);
    read_file.close();

    // ensure last char is null terminator
    file_chars[fileLength] = '\0';
    return file_chars;
}

pair<string, string> isolate_filename_and_filepath(string path_str) {
    // the last \ or / will be the one before the name of the file
    size_t filenamestart = path_str.find_last_of("/\\");
    string filepath = path_str.substr(0,filenamestart + 1);
    string filename = path_str.substr(filenamestart + 1); // here filename is [filename].tri or [filename].face
    size_t filenameend = filename.find_last_of(".");
    filename = filename.substr(0, filenameend);
    return pair<string, string>(filename, filepath); // here we remove the extension (either .tri or .face)
}