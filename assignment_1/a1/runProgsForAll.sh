#!/bin/bash
#this bash script should run ./face2faceindex and ./faceindex2directededge for every .tri file and .face file (respectively) in the given directory
# can be executed using:
# bash runProgsForAll.sh ../models/triangles

for triFile in $1/*.tri
do 
    ./face2faceindex $triFile
done

echo ""
echo "##########################"
echo "  .tri files converted"
echo "##########################"
echo ""

for faceFile in $1/*.face
do 
    ./faceindex2directededge $faceFile -m
done