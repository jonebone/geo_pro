#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <set>
#include <vector>



using namespace std;


struct vertex {
    float x, y, z;
    int id;

    bool operator==(const vertex &other) const {
        return x == other.x && y == other.y && z == other.z;
    }

    // comparisons are done using only the coordinates of the vertices so that uniqeness within the set is gauged only on coordinates and not ID
    bool operator<(const vertex &other) const {
        return x < other.x || (x == other.x && (y < other.y || (y == other.y && z < other.z)));
    }


    vertex(float xin, float yin, float zin, int idin) : x(xin), y(yin), z(zin), id(idin) {}
};


struct face {
    int v1, v2, v3;

    bool operator<(const face &other) const {
        return v1 < other.v1 || (v1 == other.v1 && (v2 < other.v2 || (v2 == other.v2 && v3 < other.v3)));
    }

    bool operator==(const face &other) const {
        return v1 == other.v1 && v2 == other.v2 && v3 == other.v3;
    }

    bool hasVertex(int v) const {
        return v1 == v || v2 == v || v3 == v;
    }

    bool hasEdge(int _v0, int _v1) const {
        return (v1 == _v0 || v2 == _v0 || v3 == _v0) && (v1 == _v1 || v2 == _v1 || v3 == _v1) && (_v0 != _v1);
    }

    face(int vv1, int vv2, int vv3) : v1(vv1), v2(vv2), v3(vv3) {}
};

struct byID {
    bool operator () (const vertex &lhs, const vertex &rhs) {
        return lhs.id <= rhs.id;
    }
};


bool face2faceindex(char * path);

char * read_file(char * path);