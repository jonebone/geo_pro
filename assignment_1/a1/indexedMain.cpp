#include "face2faceindex.h"


int main(int argc, char **argv) {

    // check the args to make sure there's an input file
	if (argc == 2) { // two parameters - convert supplied .tri file
		if (!face2faceindex(argv[1])) {
			printf("failed \n");
		}
		else { // conversion successful
            printf("done \n");
		} // surface read succeeded			
	} // two parameters - read a file

	else { // if wrong number of params
		cerr << "Unexpected number of arguments received- please run with: \n 1. first argument supplying the .tri file to be converted \n";
	}

}