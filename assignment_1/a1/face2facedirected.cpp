#include "face2facedirected.h"
#include "FileReader.h"

int eulersGenus(int numEdges, int numVertices, int numFaces) {
    // Euler's formula is V - E + F = 2(1 - g)
    // where V, E, F are numbers of Vertices, Edges, and Faces, respectively
    // and g is the genus
    //cout << "V " << numVertices << " E " << numEdges << " F " << numFaces << endl; 
    return ((numVertices - numEdges + numFaces)/-2) + 1;
}

/// pinchPointTest returns true if the point tested is a pinch point, false if the point tested is not a pinch point
bool pinchPointTest(int startIndex, int faceCount, vector<directedEdge>& edgeVector) {

    int currentIndex = startIndex;
    int goalIndex = edgeVector[startIndex].other_half;
    int faceIDx3, edgeToPointIndex;

    // currentIndex should be a directed edge from the point in question
    // let currentIndex = (3 * faceID) + i
    // where i = currentIndex % 3
    // then (3 * faceID) + (i + 2)%3 is the index of the directed edge on the same face that goes to the point in question
    // and its other half is an edge from the point in question on an adjacent face

    // faceIDx3 is 3 * faceID but since we don't need the actual faceID there's no sense in dividing faceIDx3 by 3 just to multiply it later
    
    // count how many faces are adjacent to each other forming a loop around the vertex in question
    // runs in at most n time where n is the number of faces containing the point in question
    int loopCount = 0;
    while(edgeToPointIndex != goalIndex) {
        faceIDx3 = currentIndex - (currentIndex  % 3);
        edgeToPointIndex = faceIDx3 + ((currentIndex % 3) + 2) % 3;
        currentIndex = edgeVector[edgeToPointIndex].other_half;
        if (currentIndex == -1) return true;
        loopCount ++;
    }

    // if the number of faces using the vertex differs from the number of contiguous faces around the vertex, it is a pinch point
    return loopCount != faceCount;
}



//                                              pass by reference to avoid copy costs
int findOtherHalf(int from, int to, int faceID, vector<directedEdge>& edgeVector) {
    // using "find" is at most N, so for loop is just as efficient
    // runs in N time where N is the size of edgeVector
    for (int i = 0; i < faceID; i++) {
        for (int j = 0; j < 3; j++) {
            if (edgeVector[(i * 3) + j].vert_index == to && edgeVector[(i * 3) + ((j + 1) % 3)].vert_index == from) return (i * 3) + j;
        }
    }
    // this return will only be reached if other half cannot be found
    return -1;
}

// this runs in constant time, and is just a series of constant complexity operations
void addEdgeToVector(int vertexID, int ownID, int otherHalfID, bool checkManifold, vector<directedEdge> &edgeVector) {
    // update other half and edge if other half found
    if (otherHalfID != -1) { // if other half found (not found is represented by -1)
        // update the found directedEdge in the vector
        if (edgeVector[otherHalfID].other_half != -1) {
            cerr << " - Edge " << otherHalfID << " already has other half " << edgeVector[otherHalfID].other_half << " but is also recognized as other half of " << ownID << endl;
            cerr << "    => Edge " << ownID << " will have its other half marked as -1 due to this collision\n";
            if (checkManifold) {
                cerr << " - Mesh is non-manifold \n";
                exit(EXIT_FAILURE);
            }
        } else {
            edgeVector[otherHalfID].other_half = ownID;
        }
        // add the new edge with corresponding other half to the vector
        // this has constant time for std::vector
        edgeVector.push_back(directedEdge(vertexID, otherHalfID));
    } else {
        // add new edge with no other half
        // this has constant time for std::vector
        edgeVector.push_back(directedEdge(vertexID));
    }
    return;
}



bool face2facedirected(char * path, bool checkManifold) {
    // init vectors
    vector<vertex> vert_vec;
    vector<vertex>::iterator vit;
    vector<directedEdge> edgeVector;

    //////// FILE OPENING

    // open .face file to read from
    // this method assumes clean input and does not check for unexpected characters
    char * face_file_source = read_file(path);
    /// check if the first character is the terminating character 
    // if it is, then the file read did not exist or was blank
    if (face_file_source[0] == '\0') {
        cerr << " ! File provided is empty or does not exist" << endl;
        return false;
    }

    /// separate the parts of the input filepath into:
    // filepath - the ..\folder names\ bit
    // filename - the name of the file, minus the extension- in this case dropping the .face
    /// isolating the filename this way allows the mesh to be accurately named in the header of the output file
    // as well as allowing the output file to have the same name but the new .diredge extension
    pair<string, string> file_pair = isolate_filename_and_filepath(path);
    string filepath = file_pair.second;
    string filename = file_pair.first;
    cout << filename << "\n"; 

    // open output .diredge file to write to
    string diredge_filepath = filepath + filename + ".diredge";
    ofstream diredge_file;
    diredge_file.open(&diredge_filepath[0]);
    // write header to it
    diredge_file << "# University of Leeds 2021-2022 \n# COMP 5812M Assignment 1 \n# John Barbone \n# 201262628 \n#\n# Object name: " << filename << "\n#\n"; 


    // init vertex output string
    string vertexOutputString = "";

    // init face output string
    string faceOutputString = "";
    // init face safety count
    // this will be used to confirm that the faceID's we read from file are ordered as expected 
    int faceCount = 0;

    ///// FILE PARSING
    int lineNumber = 1;
    // this while loop iterates once per line of the .face file
    char * token = strtok(face_file_source, "\n");
    while(token != NULL) {
        switch (toupper(token[0])) {
            case '#':
                // is part of header and can be ignored
                break;
            // case 'V' should be satisfied v times where v is the number of vertices in file
            // with linear time c where c is the length of the new output string, the iterations through this case have complexity cv
            // formally: n^2
            case 'V': { // if it starts with V, its a Vertex line
                    // vertex lines will be identical to those in .face file, so the tokens will be amassed and written to the output file
                    // append is of unspecified complexity, but assumed linear to size of new string
                    vertexOutputString.append(token);
                    vertexOutputString.append("\n");
                    
                    // add vertex to vertex set
                    char * temp;
                    // "Vertex" will be token[0] to token[5] so &token[6] is the string starting immediately following "Vertex"
                    // strtol and strtof both ignore leading whitespace, so they can be used on their own to split up everything following "Vertex"
                    long int id = strtol(&token[6], &temp, 10);
                    if (temp == &token[6]) { // if this is true then strtol failed to read the value
                        cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                        return false;
                    }

                    char * remaining;
                    float x = strtof(temp, &remaining);
                    if (remaining == temp) { // if this is true then strtof failed to read the value
                        cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                        return false;
                    }
                    temp = remaining;
                    float y = strtof(temp, &remaining);
                    if (remaining == temp) { // if this is true then strtof failed to read the value
                        cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                        return false;
                    }
                    temp = remaining;
                    float z = strtof(temp, &remaining);
                    if (remaining == temp) { // if this is true then strtof failed to read the value
                        cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                        return false;
                    }
                    temp = remaining;

                    // insert into vertex set the vertex with read values
                    // std::vector.push back is a constant complexity operation
                    vert_vec.push_back(vertex(x, y, z, id));
                    
                    break;
                }

            // complexity of n^2
            // for each of f faces, do 3 linear searches for other half
            case 'F': {
                // starts with F, must be Face line
                // we will reuse these lines exactly in the next file so save the raw token
                faceOutputString.append(token);
                faceOutputString.append("\n");
                //
                char * temp = &token[4];
                int faceID = strtol(&token[4], &temp, 10);
                if (&token[4] == temp) { // if this is true then strtol failed to read the value
                    cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                    return false;
                }
                // face safety check
                if (faceID != faceCount) {
                    cerr << " ! Faces not in expected numerical order: expected face " << faceCount << ", got face " << faceID << endl;
                }
                 
                // parse vertex indices involved
                char * remaining;
                int v0 = strtol(temp, &remaining, 10);
                if (remaining == temp) { // if this is true then strtol failed to read the value
                    cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                    return false;
                }
                temp = remaining;

                int v1 = strtol(temp, &remaining, 10);
                if (remaining == temp) { // if this is true then strtol failed to read the value
                    cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                    return false;
                }
                temp = remaining;

                int v2 = strtol(temp, &remaining, 10);
                if (remaining == temp) { // if this is true then strtol failed to read the value
                    cerr << " ! Unexpected format encountered in line " << lineNumber << " : " << token << endl;
                    return false;
                }
                temp = remaining;
                // update vertices face counts
                vert_vec[v0].face_count++;
                vert_vec[v1].face_count++;
                vert_vec[v2].face_count++;
                // update first directed edge of vertices if needed
                if (vert_vec[v0].first_dirEdge < 0) vert_vec[v0].first_dirEdge = 3 * faceID + 1;
                if (vert_vec[v1].first_dirEdge < 0) vert_vec[v1].first_dirEdge = 3 * faceID + 2;
                if (vert_vec[v2].first_dirEdge < 0) vert_vec[v2].first_dirEdge = 3 * faceID + 0;
                // check for other halves (if appropriate)
                // complexity of E where E is the number of directed edges in the vector
                int d0otherHalfID = findOtherHalf(v2, v0, faceID, edgeVector); 
                int d1otherHalfID = findOtherHalf(v0, v1, faceID, edgeVector); 
                int d2otherHalfID = findOtherHalf(v1, v2, faceID, edgeVector); 
                
                
                // add edges and update corresponding other half
                // constant complexity
                addEdgeToVector(v2, 3 * faceID + 0, d0otherHalfID, checkManifold, edgeVector);
                addEdgeToVector(v0, 3 * faceID + 1, d1otherHalfID, checkManifold, edgeVector);
                addEdgeToVector(v1, 3 * faceID + 2, d2otherHalfID, checkManifold, edgeVector);
                

                
                // update faceCount
                faceCount ++;
                break;
            }
            default:
                cerr << " ! read unexpected character at start of line: \n =>" << token << "\n";
        }
        lineNumber ++;
        token = strtok(NULL, "\n");
    }

    // write count of vertices and faces to output file
    diredge_file << "# Vertices=" << vert_vec.size() << " Faces=" << faceCount << "\n#\n";

    // write vertex lines to file
    diredge_file << vertexOutputString;

    // write First Directed Edge block to file
    // simultaneously do pinch point checking
    for (vertex v : vert_vec) {
        diredge_file << "FirstDirectedEdge "<< v.id << " " << v.first_dirEdge << " \n";
    }

    // wite face lines to file
    diredge_file << faceOutputString;

    // write other half block
    bool isManifold = true;
    int edgeID = 0;
    for (directedEdge de : edgeVector) {
        if (de.other_half == -1) {
            // we check if isManifold has been switched to false yet so that only the first unpaired edge is reported
            // this check is done inside the other_half == -1 check since if de.other_half != -1, then the other parts don't matter at all
            if (isManifold && checkManifold) printf(" - Edge %i remains unpaired - mesh is not manifold\n", edgeID);
            isManifold = false; 
        }
        char OtherHalfLine[120]; //oversized on purpose to avoid being undersized
        sprintf(OtherHalfLine, "OtherHalf %3i %3i\n", edgeID, de.other_half);
        diredge_file << OtherHalfLine;
        edgeID ++;
    }

    if (checkManifold) {
        if (isManifold) {
            // n^2 complexity
            // for each of v vertices do at most f ops where f is the number of faces containing that vertex
            for (vertex v : vert_vec) {
                if (pinchPointTest(v.first_dirEdge, v.face_count, edgeVector)) {
                    cerr << " - Vertex " << v.id << " failed pinch point test - mesh is not manifold \n";
                    exit(EXIT_FAILURE);
                }
            }
            printf(" - Mesh is manifold\n");
            printf(" - Euler's genus is %i \n", eulersGenus(edgeVector.size()/2, vert_vec.size(), faceCount));
        }
    }


    return true;
}
