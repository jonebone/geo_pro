#include "face2faceindex.h"
#include "FileReader.h"

bool face2faceindex(char * path) {
    // open .tri file to read from
    // this method assumes clean input and does not check for unexpected characters
    char * tri_file_source = read_file(path);
    /// check if the first character is the terminating character 
    // if it is, then the file read did not exist or was blank
    if (tri_file_source[0] == '\0') {
        cerr << "File '" << path <<"' is empty or does not exist" << endl;
        return false;
    }

    /// separate the parts of the input filepath into:
    // filepath - the ..\folder names\ bit
    // filename - the name of the file, minus the extension- in this case dropping the .face
    /// isolating the filename this way allows the mesh to be accurately named in the header of the output file
    // as well as allowing the output file to have the same name but the new .diredge extension
    pair<string, string> file_pair = isolate_filename_and_filepath(path);
    string filepath = file_pair.second;
    string filename = file_pair.first;
    cout << filename << "\n"; 


    // reads the assumed face count as the first number in the file
    char * remaining;
    long int face_count_from_file = strtol(tri_file_source, &remaining, 10);
    if (remaining == tri_file_source) { // if this is true then strtol failed to read the value
        cerr << " ! Could not read face count " << endl;
        return false;
    }


    // vertex and face parsing code:
    // using sets prevents duplicates
    set<vertex> vset;
    set<vertex>::iterator vit;
    pair<set<vertex>::iterator, bool> vin;
    // faces can be a vector as duplicates are not expected, and insertion into a vector is constant
    vector<face> faceVec;
    int vCount = 0;
    int vIDarr[3];
    int coordCount = 0;
    float coord[3];
    char * start = NULL;

    /// use while(true) here since the condition to break can only be tested once the first lines of the while loop have been executed
    // however, this condition also needs to be evaluated before allowing the contents of the loop following it to be executed 
    /// the complexity of this loop is nlog(n) where n is the number of vertices in the file
    // however, this loop runs 3 times per vertex, with every 3rd iteration running in log(n) time while all others run in constant time
    // every 9th iteration of this while loop performs additional operations for adding a face to the vector of faces, all of which are of constant complexity
    while (true) {
        // remaining is a pointer to the first element of the input string that has not been parsed
        start = remaining;
        // strtof(x, &y) ignores all whitespace characters starting with the character pointed to by x, until it encounters characters parsable into a float
        // once it encounters a character that is no longer parseable into a float, it returns the parsed float and sets y to point to the character it stopped at
        // assuming whitespace is the delimiter between all floats, remaining will be a pointer to the whitespace preceding the next float
        coord[coordCount % 3] = strtof(start, &remaining);

        // if start and remaining are identical pointers, it is because strtof could not find a float to parse.
        // this will ideally only be the case once we have parsed all vertices, and it encounters the terminating character
        // but could also happen if an unexpected character is encountered
        if (start == remaining) {
            // strtok returns the next token delimited by the supplied string (in this case, whitespace)
            // and returns NULL if no such token exits
            // thus, strok is used to verify that a float was not found by strtof because there is nothing left to parse
            // if an unexpected character or token is encountered, strtok will not be NULL but rather the unexpected symbol
            char* testToken = strtok(remaining, " \n");
            if(testToken != NULL) {
                cerr << " ! Unexpected token encountered: " << testToken << endl;
                return false;
            }
            break;
        }

        // if coordCount is 2, then we have coords 0, 1, and 2 - enough to create a vertex
        if (coordCount % 3 == 2) {
            vertex v = vertex(coord[0], coord[1], coord[2], (int)vset.size());
            // vin is two parts- iterator pointing at inserted element or collision element, and bool indicating if insertion was without collision (true) or if element collided (false)
            vin = vset.insert(v);
            /// formally, the complexity of this insert in context is nlog(n) where n is the number of vertices in the file
            // insertion into std::set is log(n) where n is the size of the set
            // thus the complexity of inserting n unique elements into an empty set is nlog(n)
            // however, practically we know that in the minimum manifold mesh, a tetrahedron, the .tri file will have each vertex listed 3 times - once for each face that contains it
            // any other manifold mesh must have at least 3 faces per vertex, so from n vertices there will be at most n/3 unique vertices
            // thus a manifold mesh will run in at most nlog(n/3) time, where n is the number of vertices in the file 

            // if we have inserted successfully (i.e. the vertex is unique), add the vertex ID to the array
            // this boolean (vin.second) is a product of our insertion and comes at no extra cost, and indexing into an array is constant complexity
            if (vin.second) vIDarr[vCount % 3] = v.id;
            // else set the 'vit' iterator position to that of the vertex in the set that caused collision
            else {
                // this iterator is a product of the earler insertion and comes at no extra complexity cost
                vit = vin.first;
                // we get the ID of the vertex that is already in the set, and add that ID to the array of ID's that make up our next face
                vertex temp = *vit;
                // indexing is constant complexity
                vIDarr[vCount % 3] = temp.id;
            }
            // if vCount % 3== 2, then vIDarr[] is full and ready to make a new face
            if (vCount % 3 == 2) {
                face f = face(vIDarr[0], vIDarr[1], vIDarr[2]);
                // std::vector.push_back is constant complexity
                faceVec.push_back(f);
            } // end of adding face to vector

            vCount ++; 
        } // end of adding vertex to set
        
        coordCount ++; 
    } // end of while loop

    // end of parsing / creating set of vertices and vector of faces


    /// safety checking number of vertices and faces
    if (coordCount % 3 != 0) {
        cerr << " ! read " << coordCount << " coordinates, which is not divisible by 3 \n";
        return false;
    }

    if (vCount % 3 != 0) {
        cerr << " ! read " << vCount << " vertices, which is not divisible by 3 \n";
        return false;
    }

    // open output .face file to write to
    string face_filepath = filepath + filename + ".face";
    ofstream face_file;
    face_file.open(&face_filepath[0]);
    if (!face_file) {
        cerr << " ! Could not open file '" << face_filepath << "' \n";
        return false;
    }

    face_file << "# University of Leeds 2021-2022 \n# COMP 5812M Assignment 1 \n# John Barbone \n# 201262628 \n#\n# Object name: " << filename << "\n#\n"; 
    face_file << "# Vertices=" << vset.size() << " Faces=" << faceVec.size() << "\n#\n";

    // write vertices to file
    // first re-sort vertices by ID
    // this is complexity nlog(n) since insert for a set in log(n) and we insert n values
    // where n is the number of vertices in the set
    set<vertex, byID> idSortedVertices;
    for (vertex temp : vset) {
        idSortedVertices.insert(temp);
    }

    // write vertices to file in ID order
    // this is complexity n since only constant complexity things are done for each of n elements
    // where n is the number of vertices in the set
    for (vertex temp : idSortedVertices) {
        // format output line
        char fileline[160];
        // format numbers to be %<width>.<precision>
        sprintf(fileline, "Vertex %-3i %10.7f %10.7f %10.7f \n", temp.id, temp.x, temp.y, temp.z);
        face_file << fileline;
    }
    
    //write faces to file
    // this is complexity n since only constant complexity things are done for each of n elements
    // where n is the number of faces
    int faceID = 0;
    for (face temp : faceVec) {
        char fileline [160];
        sprintf(fileline, "Face %-3i %3i %3i %3i\n", faceID, temp.v1, temp.v2, temp.v3);
        face_file << fileline;
        faceID ++;
    }

    if (faceID != (int)face_count_from_file) {
        cerr << " ! faces parsed does not match face count in file- expected " << face_count_from_file << " got " << faceID << endl;
    }

    

    return true;
}
