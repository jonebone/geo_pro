#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <set>
#include <queue>
#include <vector>


using namespace std;

struct vertex {
    float x, y, z;
    int id;
    mutable int first_dirEdge;
    // counting how many faces use each point as faces are parsed saves re-counting later during the pinch point test
    mutable int face_count;
    

    // no longer truly need this but it allows functionality should a sort be needed
    bool operator<(const vertex &other) const {
        return id < other.id;
    }

    bool operator==(const vertex &other) const {
        return x == other.x && y == other.y && z == other.z;
    }

    vertex(float xin, float yin, float zin, int idin) : x(xin), y(yin), z(zin), id(idin) {
        first_dirEdge = -1;
        face_count = 0;
    }
};


struct directedEdge {
    int vert_index;
    int other_half;

    // other half being set to -1 indicates that the other half is missing
    directedEdge(int _vert_index) : vert_index(_vert_index) {
        other_half = -1;
    }

    directedEdge(int _vert_index, int _other_half) : vert_index(_vert_index), other_half(_other_half) {}
    
};

int eulersGenus(int numEdges, int numVertices, int numFaces);

bool pinchPointTest(int startIndex, int faceCount, vector<directedEdge>& edgeVector);

//                                              pass by reference to avoid copy costs
int findOtherHalf(int from, int to, int faceID, vector<directedEdge>& edgeVector);


bool face2facedirected(char * path, bool checkManifold);

void addEdgeToVector(int vertexID, int ownID, int otherHalfID, bool checkManifold, vector<directedEdge> &edgeVector);
