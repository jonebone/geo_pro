Compilation/running this code is almost exactly as it was when distributed

notes on the UI:
- normals shown are not actually unit length, instead only indicating direction each points from its corresponding vertex
- "Flip Normals" exists due to many of the provided .diredgenormal files having normals facing inwards instead of outwards.
    flipping the normals with this checkbox only changes how the normals are displayed, not how they are stored. 

notes on the numbering:
- numbering of edges and first directed edges matches that of the source files
- directed edge 0 on a face goes from its third vertex to its first vertex, edge 1 goes from first to second, and edge 2 from second to third
- first directed edge of a vertex is the lowest index directed edge FROM that vertex
- when face f is subdivided, the central new face becomes face f

To compile on feng-linux / feng-gps:

module add qt/5.13.0
qmake -project QT+=opengl
qmake
make

To compile on OSX:
Use Homebrew to install qt

qmake -project QT+=opengl
qmake
make

To compile on Windows:
Unfortunately, the official OpenGL on Windows was locked at GL 1.1.  Many many hacks exist, and they all disagree.
Just to make it worse, the Qt response to this is clumsy.  Net result: there is no easy way to get this compiling on Windows.
I will aim to update these instructions at a later date.

To run on feng-linux / feng-gps with no saving:

./LoopSubdivisionRelease ../path_to/model.diredgenormal 

To run on feng-linux / feng-gps with saving to file:

./LoopSubdivisionRelease ../path_to/model.diredgenormal ../path_to/save_file_name



To run on OSX with no saving:
./LoopSubdivisionRelease.app/Contents/MacOS/FakeGLRenderWindowRelease  ../path_to/model.diredgenormal

To run on OSX with saving to file:
./LoopSubdivisionRelease.app/Contents/MacOS/FakeGLRenderWindowRelease  ../path_to/model.diredgenormal ../path_to/save_file_name


NB: save file does NOT need to exist prior to running the application
    the same file can be used for read and write if it is provided for both arguments, but it WILL be overwritten when the program is closed.

