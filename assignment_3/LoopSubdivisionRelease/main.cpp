//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  main.cpp
//  -----------------------------
//  
//  Loads assets, then passes them to the render window. This is very far
//  from the only way of doing it.
//  
////////////////////////////////////////////////////////////////////////

// system libraries
#include <iostream>
#include <fstream>

// QT
#include <QApplication>

// local includes
#include "RenderWindow.h"
#include "DirectedEdgeSurface.h"
#include "RenderParameters.h"
#include "RenderController.h"

// main routine
int main(int argc, char **argv) { // main()
    // initialize QT
    QApplication renderApp(argc, argv);

    // check the args to make sure there's an input file
    if (argc != 2 && argc != 3) { // bad arg count
        // print an error message
        std::cerr << "Please run with input file path as arg1 and optional arg2 as save path" << std::endl;
        //std::cout << "Usage: " << argv[0] << " geometry" << std::endl; 
        // and leave
        return 0;
    } // bad arg count

    //  use the argument to create a height field &c.
    DirectedEdgeSurface DirectedEdgeSurface;

    // open the input files for the geometry & texture
    std::ifstream geometryFile(argv[1]);

    // try reading it
    if (!(geometryFile.good()) || (!DirectedEdgeSurface.ReadObjectStream(geometryFile))) { // object read failed 
        std::cerr << "Read failed for object " << argv[1] << std::endl;
        return 0;
    } // object read failed

    if (argc == 3) {
        // create the output filepath
        std::ofstream outputFile(argv[2]);
        // ideally this would be done via a line outside of the if that is just "std::ofstream outputFile;" and separate line constructing it inside the if, so that outputFile isn't scoped just to this if
        // but feng doesn't like the line "outputFile = std::ofstream(argv[2]);"  :(

        // test before running application bc if results cannot be saved the user should know that before they produce something they might really want to save
        if (!outputFile.good()) { // if bad, alert the user
            std::cerr << "Cannot use save file " << argv[2] << std::endl;
            return 0;
        }
    }


    // subdivide before showing
    //DirectedEdgeSurface.Subdivide();
    //if (!DirectedEdgeSurface.HalfEdgeCheck()) std::cout << "half edges bad :(" << std::endl;

    // dump the file to out
//     DirectedEdgeSurface.WriteObjectStream(std::cout);

    // create some default render parameters
    RenderParameters renderParameters;

    // use the object & parameters to create a window
    RenderWindow renderWindow(&DirectedEdgeSurface, &renderParameters, argv[1]);

    // create a controller for the window
    RenderController renderController(&DirectedEdgeSurface, &renderParameters, &renderWindow);

    //  set the initial size
    renderWindow.resize(762, 664);

    // show the window
    renderWindow.show();

    // set QT running
    int ret = renderApp.exec();

    


    // after exec, write file (if applicable)
    if (argc == 3) {
        std::ofstream outputFile(argv[2]); // we know its good from before
        DirectedEdgeSurface.WriteObjectStream(outputFile);
        outputFile.close();
    }

    return ret;
} // main()
