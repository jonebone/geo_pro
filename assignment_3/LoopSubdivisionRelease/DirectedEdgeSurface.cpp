///////////////////////////////////////////////////
//
//  Hamish Carr
//  September, 2020
//
//  ------------------------
//  DirectedEdgeSurface.cpp
//  ------------------------
//  
//  Base code for rendering assignments.
//
//  Minimalist (non-optimised) code for reading and 
//  rendering an object file
//  
//  We will make some hard assumptions about input file
//  quality. We will not check for manifoldness or 
//  normal direction, &c.  And if it doesn't work on 
//  all object files, that's fine.
//
//  While I could set it up to use QImage for textures,
//  I want this code to be reusable without Qt, so I 
//  shall make a hard assumption that textures are in 
//  ASCII PPM and use my own code to read them
//  
///////////////////////////////////////////////////

// include the header file
#include "DirectedEdgeSurface.h"

// include the C++ standard libraries we want
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "math.h"

// include the Cartesian 3- vector class
#include "Cartesian3.h"
#include "SphereVertices.h"

#define MAXIMUM_LINE_LENGTH 1024

// constructor will initialise to safe values
DirectedEdgeSurface::DirectedEdgeSurface()
    : centreOfGravity(0.0,0.0,0.0)
    { // DirectedEdgeSurface()
    // force arrays to size 0
    vertices.resize(0);
    normals.resize(0);
	firstDirectedEdge.resize(0);
	faceVertices.resize(0);
	otherHalf.resize(0);
} // DirectedEdgeSurface()

// read routine returns true on success, failure otherwise
bool DirectedEdgeSurface::ReadObjectStream(std::istream &geometryStream) { // ReadObjectStream()
    
    // create a read buffer
    char readBuffer[MAXIMUM_LINE_LENGTH];

	// token for identifying meaning of line
	std::string token;
    
    // the rest of this is a loop reading lines & adding them in appropriate places
    while (geometryStream >> token) { // while can read token (stops if last char is newline)
	
        // check for eof() in case we've run out
        if (geometryStream.eof())
            break;

        // otherwise, switch on the token we read
		if (token == "#") { // comment 
			// read and discard the line
			geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
        } // comment
		else if (token == "Vertex") { // vertex
			// variables for the read
			unsigned int vertexID;
			geometryStream >> vertexID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (vertexID != vertices.size())
				{ // bad vertex ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad vertex ID				
			
			// read in the new vertex position
			Cartesian3 newVertex;
			geometryStream >> newVertex;
			
			// and add it to the vertices
			vertices.push_back(newVertex);
		} // vertex
		else if (token == "Normal") { // normal
			// variables for the read
			unsigned int normalID;
			geometryStream >> normalID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (normalID != normals.size()) { // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
			} // bad ID				
			
			// read in the new normal
			Cartesian3 newNormal;
			geometryStream >> newNormal;
			
			// and add it to the vertices
			normals.push_back(newNormal);
		} // normal
		else if (token == "FirstDirectedEdge") { // first directed edge
			// variables for the read
			unsigned int FDEID;
			geometryStream >> FDEID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (FDEID != firstDirectedEdge.size())
				{ // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad ID				
			
			// read in the new FDE
			unsigned int newFDE;
			geometryStream >> newFDE;
			
			// and add it to the vertices
			firstDirectedEdge.push_back(newFDE);
		} // first directed edge
		else if (token == "Face") { // face
			// variables for the read
			unsigned int faceID;
			geometryStream >> faceID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (faceID != faceVertices.size()/3) { // bad face ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
			} // bad face ID				
			
			// read in the new face vertex (3 times)
			unsigned int newFaceVertex;
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
		} // face
		else if (token == "OtherHalf") { // other half
			// variables for the read
			unsigned int otherHalfID;
			geometryStream >> otherHalfID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (otherHalfID != otherHalf.size()) { // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
			} // bad ID				
			
			// read in the new face vertex (3 times)
			unsigned int newOtherHalf;
			geometryStream >> newOtherHalf;
			otherHalf.push_back(newOtherHalf);
		} // other half
    } // not eof

    // compute centre of gravity
    // note that very large files may have numerical problems with this
    centreOfGravity = Cartesian3(0.0, 0.0, 0.0);

    // if there are any vertices at all
    if (vertices.size() != 0) { // non-empty vertex set
        // sum up all of the vertex positions
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
            centreOfGravity = centreOfGravity + vertices[vertex];
        
        // and divide through by the number to get the average position
        // also known as the barycentre
        centreOfGravity = centreOfGravity / vertices.size();

        // start with 0 radius
        objectSize = 0.0;

        // now compute the largest distance from the origin to a vertex
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++) { // per vertex
            // compute the distance from the barycentre
            float distance = (vertices[vertex] - centreOfGravity).length();         
            
            // now test for maximality
            if (distance > objectSize)
                objectSize = distance;
        } // per vertex
    } // non-empty vertex set

    // return a success code
    return true;
} // ReadObjectStream()

// write routine
void DirectedEdgeSurface::WriteObjectStream(std::ostream &geometryStream) { // WriteObjectStream()
	geometryStream << "#" << std::endl; 
	geometryStream << "# Created for Leeds COMP 5821M Autumn 2020" << std::endl; 
	geometryStream << "#" << std::endl; 
	geometryStream << "#" << std::endl; 
	geometryStream << "# Surface vertices=" << vertices.size() << " faces=" << faceVertices.size()/3 << std::endl; 
	geometryStream << "#" << std::endl; 

	// output the vertices
    for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
        geometryStream << "Vertex " << vertex << " " << std::fixed << vertices[vertex] << std::endl;

    // and the normal vectors
    for (unsigned int normal = 0; normal < normals.size(); normal++)
        geometryStream << "Normal " << normal << " " << std::fixed << normals[normal] << std::endl;

	// and the first directed edges
    for (unsigned int vertex = 0; vertex < firstDirectedEdge.size(); vertex++)
        geometryStream << "FirstDirectedEdge " << vertex<< " " << std::fixed << firstDirectedEdge[vertex] << std::endl;

    // and the faces - increment is taken care of internally
    for (unsigned int face = 0; face < faceVertices.size(); ) { // per face
        geometryStream << "Face " << face/3 << " ";
        
        // write the three vertices for that face
        geometryStream << faceVertices[face++] << " ";
        geometryStream << faceVertices[face++] << " ";
        geometryStream << faceVertices[face++];
            
        geometryStream << std::endl;
    } // per face

	// and the other halves
	for (unsigned int dirEdge = 0; dirEdge < otherHalf.size(); dirEdge++)
		geometryStream << "OtherHalf " << dirEdge << " " << otherHalf[dirEdge] << std::endl;
	
} // WriteObjectStream()

// routine to render
void DirectedEdgeSurface::Render(RenderParameters *renderParameters) { // Render()
    // Ideally, we would apply a global transformation to the object, but sadly that breaks down
    // when we want to scale things, as unless we normalise the normal vectors, we end up affecting
    // the illumination.  Known solutions include:
    // 1.   Normalising the normal vectors
    // 2.   Explicitly dividing the normal vectors by the scale to balance
    // 3.   Scaling only the vertex position (slower, but safer)
    // 4.   Not allowing spatial zoom (note: sniper scopes are a modified projection matrix)
    //
    // Inside a game engine, zoom usually doesn't apply. Normalisation of normal vectors is expensive,
    // so we will choose option 2.  

	// normals on some objects are flipped so allow user to graphically flip them
	float normalMod = 1.f;
	if (renderParameters->flipNormals) normalMod = -1.f;

    // Scale defaults to the zoom setting
    float scale = renderParameters->zoomScale;
    scale /= objectSize;
        
    //  now scale everything
    glScalef(scale, scale, scale);

    // apply the translation to the centre of the object if requested
    glTranslatef(-centreOfGravity.x, -centreOfGravity.y, -centreOfGravity.z);

    // start rendering
    glBegin(GL_TRIANGLES);

	// set colour for pick render - ignored for regular render
	glColor3f(1.0, 1.0, 1.0);

    // loop through the faces
	for (unsigned int face = 0; face < faceVertices.size(); face +=3) { // per face
		// if we want flat normals, compute them here
		if (renderParameters->useFlatNormals) { // flat normals
			// find two vectors along edges of the triangle
			Cartesian3 pq = vertices[faceVertices[face+1]] - vertices[faceVertices[face]];
			Cartesian3 pr = vertices[faceVertices[face+2]] - vertices[faceVertices[face]];

			// take their cross product and normalise
			Cartesian3 faceNormal = pq.cross(pr).unit();

			// and use it to set the glNormal
			glNormal3f(faceNormal.x * scale, faceNormal.y * scale, faceNormal.z * scale);
		} // flat normals

		// we have made a HARD assumption that we have enough normals
		for (unsigned int vertex = face; vertex < face+3; vertex++) { // per vertex
		
			// if we are using smooth normals
			if (!renderParameters->useFlatNormals)
				// set the normal vector
				glNormal3f (
					normals[faceVertices[vertex]].x * scale * normalMod,
					normals[faceVertices[vertex]].y * scale * normalMod,
					normals[faceVertices[vertex]].z * scale * normalMod
				);
			
			// and set the vertex position
			glVertex3f (
				vertices[faceVertices[vertex]].x,
				vertices[faceVertices[vertex]].y,
				vertices[faceVertices[vertex]].z
			);

		} // per vertex

	} // per face

    // close off the triangles
    glEnd();
    
    // now we add a second loop to render the vertices if desired
    if (!renderParameters->showVertices && !renderParameters->showNormals) return;

	glDisable(GL_LIGHTING);

	// loop through the vertices
	for (unsigned int vertex = 0; vertex < vertices.size(); vertex++) { // per vertex
		// use modelview matrix (not most efficient solution, but quickest to code)
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(vertices[vertex].x, vertices[vertex].y, vertices[vertex].z);
		if (renderParameters->showVertices) {
			glPushMatrix();
			glColor3f(1.0, 1.0, 1.0);
			glScalef(0.1 * renderParameters->vertexSize, 0.1 * renderParameters->vertexSize, 0.1 * renderParameters->vertexSize);
			renderTriangulatedSphere();
			glPopMatrix();
		}
		if (renderParameters->showNormals) {
			glColor3f(0.2, 0.5, 1.0);
			glBegin(GL_LINES);
			glVertex3f(0., 0., 0.);
			glVertex3f(normals[vertex].x * scale * normalMod, normals[vertex].y * scale * normalMod, normals[vertex].z * scale * normalMod);
			glEnd();
		}
		glPopMatrix();
	} // per vertex 
    
} // Render()

void DirectedEdgeSurface::Subdivide() { // Subdivide()
	int startingfaces = faceVertices.size()/3;
	int startingVerts = vertices.size();
	int numVerts = vertices.size();
	int numNorms = normals.size();
	// firstDirectedEdge : index = vert index; int val = first edge from that vertex
	// directed edge 0 goes to vertex 0; edge 1 goes to vertex 1; 

	// set new positions of original vertices based on their original neighbors
	std::vector<Cartesian3> oldVertnewPos;
	oldVertnewPos.resize(vertices.size());
	// loop through starting vertices
	for (int v = 0; v < vertices.size(); v++) {
		
		Cartesian3 neighbors;
		float n = 1.f;

		unsigned int startingEdge = firstDirectedEdge[v];
		unsigned int OH = otherHalf[startingEdge];
		unsigned int nextEdge = OH - OH%3 + (OH%3 + 1)%3;
		
		neighbors = vertices[faceVertices[startingEdge]];

		// iterate through neighbors
		while(nextEdge != startingEdge) {
			neighbors = neighbors + vertices[faceVertices[nextEdge]];
			n += 1.f;
			// go next
			OH = otherHalf[nextEdge];
			nextEdge = OH - OH%3 + (OH%3 + 1)%3;

			// safety:
			if (n > vertices.size()) {
				std::cout << "while loop endless on vertex " << v << std::endl;
				return;
			}
		}

		float a;
		if (n == 3.f) a = 0.1875f;
		else { // a gets more complex, break it apart for legibility
			float cosTerm = 0.375 + 0.25 * cos((2. * M_PI) / n);
			a = (0.625 - cosTerm * cosTerm) / n;
		}

		// update position of vertex v
		oldVertnewPos[v] = vertices[v] * (1.f - n * a) + neighbors * a;

	}

	std::cout << "p1 done \n";

	// add 1 vertex for every 2 directed half edges or 1 vertex per edge
	vertices.resize(vertices.size() + otherHalf.size()/2); 

	// normals same capacity as vertices
	normals.resize(vertices.capacity());

	// firstDirectedEdge : index = vert index; int val = first edge from that vertex
	// as it is in the provided files

	

	// starting with x faces => end with 4x faces
	// starting with 3x edges for x faces => 3 * 4 * x edges
	faceVertices.resize(startingfaces * 4 * 3); // * 3 to undo division earlier
	otherHalf.resize(   startingfaces * 4 * 3); // * 3 bc 3 half edges per face

	// rewrite firstDirectedEdge vector 
	firstDirectedEdge.clear();
	// init to size of vertices, with number of edges as default val
	// this works great bc the max edge index is one less than the number of edges
	firstDirectedEdge.resize(vertices.size(), otherHalf.size());


	for (int i = 0; i < startingfaces; i ++) { 
		// old vertex indices
		int oldVerts[3] = {faceVertices[3 * i], faceVertices[3 * i + 1], faceVertices[3 * i + 2]};
		
		// new vertex indices
		int newVerts[3] = {0, 0, 0}; // will all be changed later, just good to init to some value

		for (int j = 0; j < 3; j ++) {
			int H = otherHalf[3 * i + j];
			// check if new vertex already exists along a half edge E by seeing if the other half H of E still has E as its (H's) other half
			// edges 0,1,2  on face f are edge ids 3f + 0, 3f + 1, 3f + 2, respectively
			if ((int)otherHalf[H] != (3 * i + j)) { // if not equal, new vertex exists already
				// other half H will be on face (H-H%3)/3, pointing to the vertex H%3 (0, 1, or 2) of that face
				// thus the new vertex index for vertex j on face I should be the vertex found at 
				// faceVertices[ 3*( (H-H%3)/3 ) + H%3 ] or faceVertices[ H ]
				//
				newVerts[j] = faceVertices[ H ];
			} else {
			 // otherwise, make a new vertex on that edge 
				// since diredges on a face start with going last to first, first to second, second to third
				// new subdivided vertices (vi, vii, viii) should start with vi between last and first, vii between first and second, and viii between second and third
				newVerts[j] = numVerts;
				Cartesian3 ownFaceFarVertex = vertices[oldVerts[mod3(j + 1)]];
				Cartesian3 adjFaceFarVertex = vertices[faceVertices[H - H%3 + (H%3 + 1)%3 ]];
				vertices[numVerts] 	= (vertices[oldVerts[mod3(j - 1)]] + vertices[oldVerts[(j)]] ) * 0.375f
									+ (vertices[oldVerts[mod3(j - 2)]] + vertices[faceVertices[H - H%3 + (H%3 + 1)%3 ]]) * 0.125f; 
				normals[numNorms]   = (normals[oldVerts[mod3(j - 1)]] + normals[oldVerts[(j)]] ) * 0.375f
									+ (normals[oldVerts[mod3(j - 2)]] + normals[faceVertices[H - H%3 + (H%3 + 1)%3 ]]) * 0.125f;
				normals[numNorms] 	= normals[numNorms].unit();
				
				numVerts++;
				numNorms++;
			}


			// face ID i becomes central face; face ID i has the three new vertices as its vertices
			faceVertices[3 * i + j] = newVerts[j];

			// firstDirectedEdge : index = vertex index; int val = first edge from that vertex
			// the vertex at faceVertices[3i + j] has edge 3i + (j + 1)%3 going from it
			// so if the value at firstDirectedEdge[faceVertices[3i + j]] is greater than 3i + (j + 1)%3, then it should be replaced by 3i + (j+1)%3
			if ( firstDirectedEdge[faceVertices[3 * i + j]] > 3 * i + (j + 1)%3 )  // safe to use %3 instead of mod3 bc (j+1) will always be greater than 0
				firstDirectedEdge[faceVertices[3 * i + j]] = 3 * i + (j + 1)%3;

			// new other halfs

			// face #i of x faces => face i, face x + 3i + 0, face x + 3i + 1, face x + 3i + 2 
			// 	with 3 edges per face, edges e_0, e_1, e_2 on face i have indices 3i + 1, 3i + 2, 3i + 3, respectively. 
			// when face i starts with edge 3i + j, new face IDs startingfaces + 3i + j and startingfaces + 3i + (j-1)%3 each need half of edge ID 3i+j
			// thus: 
			// edge formerly known as 3i + j becomes edges 3*(startingfaces + 3i + j) + 0 and 3*(startingfaces + 3i + (j - 1)%3) + 1
			// since:
			// edge 3i+j had previously gone to one old vertex from another old vertex (let's call them v' and v", respectively)
			// and since the old vertex is always the first listed on the new faces,
			// one new face will contain the half of the old edge 3i+j going to the old vertex v', which is v_0 for that face, 
			// 		so the new edge will be e_0 for that face
			// the other will contain the half of the old edge 3i+j coming from the old vertex v", which is v_0 for that face,
			// 		so the new edge will be going from v_0 to v_1, and thus will be e_1 for that face

			// simlarly, consider edge H, the old other half of edge 3i+j
			// H can be said to be on face (H-H%3)/3, and can be said to be edge H%3 on face (H-H%3)/3, just as edge 3i+j is edge j on face i
			// so 3i = H-H%3 and j = H%3
			// thus, H will become edges 3*(startingfaces + H-H%3 + H%3) + 0 and 3*(startingfaces + H-H%3 + (H%3 - 1)%3 ) + 1
			//  the first can be simplified to 3*(startingfaces + H)
			// 3*(startingfaces + 3i + j) 					will be the other half of 	3*(startingfaces + H-H%3 + (H%3 - 1)%3 ) + 1
			// 3*(startingfaces + 3i + (j - 1)%3) + 1	 	will be the other half of 	3*(startingfaces + H)
			otherHalf[3*(startingfaces + 3 * i + j)	]    				=  	3*(startingfaces + H - mod3(H) + mod3( mod3(H) - 1 ) ) + 1;
			otherHalf[3*(startingfaces + 3 * i + mod3(j - 1) ) + 1 ]  	= 	3*(startingfaces + H);


			// update edge that had been 3i+j and the edge that will become its other half
			otherHalf[3 * i + j] = 3*(startingfaces + 3 * i + mod3(j - 1) ) + 2;
			otherHalf[3*(startingfaces + 3 * i + mod3(j - 1) ) + 2] = 3 * i + j;

		}
		
		//std::cout << newVerts[0] << " " << newVerts[1] << " " << newVerts[2] << " \n";

		// assigning other faces loop
		// this can only be done after all newVerts values are assured
		for (int j = 0; j < 3; j ++) {
			// face #f of x faces => face #f, face x + 3f + 0, face x + 3f + 1, face x + 3f + 2 
			// face x + 3f + 0 will use original first original vertex, second new vertex, first new vertex
			// face x + 3f + 1 will use original second original vertex, third new vertex, second new vertex
			// face x + 3f + 2 will use original third original vertex, first new vertex , third new vertex
			int newFaceIndex = startingfaces + 3 * i + j;
			faceVertices[newFaceIndex * 3 + 0] = oldVerts[j];
			faceVertices[newFaceIndex * 3 + 1] = newVerts[(j + 1) % 3];
			faceVertices[newFaceIndex * 3 + 2] = newVerts[j];

			// firstDirectedEdge : index = vertex index; int val = first edge from that vertex
			// the vertex at faceVertices[3 * newFaceIndex + 0] has edge 3 * newFaceIndex + 1 going from it
			// so if the value at firstDirectedEdge[faceVertices[3 * newFaceIndex + j]] is greater than 3 * newFaceIndex + 1, then it should be replaced by 3 * newFaceIndex + 1
			// similarly vertex at faceVertices[3 * newFaceIndex + 1] has edge 3 * newFaceIndex + 2 going from it
			// and vertex at faceVertices[3 * newFaceIndex + 2] has edge 3 * newFaceIndex + 0 going from it
			if ( firstDirectedEdge[faceVertices[newFaceIndex * 3 + 0]] > newFaceIndex * 3 + 1 ) 
				firstDirectedEdge[faceVertices[newFaceIndex * 3 + 0]] = newFaceIndex * 3 + 1;
			if ( firstDirectedEdge[faceVertices[newFaceIndex * 3 + 1]] > newFaceIndex * 3 + 2 ) 
				firstDirectedEdge[faceVertices[newFaceIndex * 3 + 1]] = newFaceIndex * 3 + 2;
			if ( firstDirectedEdge[faceVertices[newFaceIndex * 3 + 2]] > newFaceIndex * 3 + 0 ) 
				firstDirectedEdge[faceVertices[newFaceIndex * 3 + 2]] = newFaceIndex * 3 + 0;
		}

	} // end face loop

	// loop through starting vertices, adjusting position
	for (int v = 0; v < startingVerts; v++)
		vertices[v] = oldVertnewPos[v];

	std::cout << "we good lmao\n" << faceVertices.size() << std::endl;

	if (!HalfEdgeCheck()) std::cout << "half edges bad :(" << std::endl;
	else std::cout << "half edges ok \n";

} // Subdivide()

bool DirectedEdgeSurface::HalfEdgeCheck() {
	int numEdges = otherHalf.size();
	
	for (int i = 0; i < numEdges; i ++) if (otherHalf[otherHalf[i]] != i) return false;
		
	return true;
}