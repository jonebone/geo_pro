# README for code distributed for use in COMP 5821M 2021-22
#
#

To compile, execute the following on feng-linux:
module add qt/5.13.0
qmake -project QT+=opengl
qmake
make

=============================================================
To run, run ./assignment_2 with .obj filepath as argument

Texture will be output as "colorTexture.ppm" in the current directory

Normal map will be output as "normalMap.ppm" in the current directory

careful- running the program multiple times will overwrite these textures and only the final ones will remain
